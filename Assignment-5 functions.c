 // program to calculate relationship between profit and ticket price
#include <stdio.h>

 //calculating income 
 int income (int tprice, int atten);
 //calculating expenditure 
 int expenditure( int atten);  
 //calculating profit 
 int profit(int i,int ex);  
 //calculating attendees 
 int attendees(int ticketprice);
 //showing relationship between ticket price and profit
 void relationship();
 
 
 int income (int tprice, int atten){  
 	return tprice*atten;
 }
 
 int expenditure( int atten){     //atten = no of attendees
 	return 500+(3*atten);
 }
  
 int profit(int i,int ex){   // i=income    ex= expenditure
 	return i-ex;
 }
 
 int attendees(int ticketprice){
 	return 120-((ticketprice -15)/5*20);
 }
 
 void relationship(){
 	int a,t,maxp=0, maxt=0, prof ;      
	
 	printf("\tPrice \t Profit \n");  
    for(t=5; t<=50;t+=5){
			a = attendees(t);   // a=no of attendees   ,   t=ticket price         
			prof = profit(income(t,a),expenditure(a));  
			
 	    	if(maxp<prof){
 	    	     maxp = prof; //maxp = maximum profit
				 maxt =t;	 //maxt = maximum ticket price for the heighest profit 
		 }	
		 printf("\tRs.%d \t Rs.%d \n",t,prof);  	     
}
    printf("\n***Above chart shows the relationship between ticketprice and profit. Profit increases up to Rs.25 ticket at a decreasing rate.***\n ");  	  
	printf("\n Heighest profit is Rs.%d  \n Heighest profit gives by Rs.%d ticket",maxp, maxt);  
   
 }
 
 int main(){
 	  
 		relationship();
 		return 0;
 }
